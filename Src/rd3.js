/**
 * Created by shabeermothi on 03/10/14.
 */

(function () {

    var rd3 = window.rd3 || {};

    rd3.version = '0.0.1b'; // Version. Currently in beta.
    rd3.dev = true;  // Sets the environment being used. Set false when being used in production

    window.rd3 = rd3;

    // Source File

})();