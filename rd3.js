/**
 * Created by shabeermothi on 03/10/14.
 */

(function () {

    var rd3 = window.rd3 || {};

    rd3.version = '0.0.2b'; // Version. Currently in beta.
    rd3.dev = true;  // Sets the environment being used. Set false when being used in production

    window.rd3 = rd3;

    rd3.charts = {}; // Chart Object which will create respective chart types
    rd3.log = {}; // Log object for RD3 Library

})();