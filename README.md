RD3
===

RD3 - Reusable D3 is a Javascript framework which allows users to draw big data visualisations with D3 without having to write hundreds of lines of code.

RD3 With Angular JS
===================

RD3 also provides Angular Directives which makes developers life simple by creating visualisations supported by RD3 as HTML tags.
To understand further on usage please refer the subfolder "Angular Components" under "Exmaples" folder for working examples.