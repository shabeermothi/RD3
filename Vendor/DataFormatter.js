/**
 * Created by shabeermothi on 07/09/14.
 */

// DataFormatter for series charts
var SeriesChartsDataFormatter;

SeriesChartsDataFormatter = {
    responseData: "",
    getUniqueValues: function (value, index, self) {
        return self.indexOf(value) === index;
    },
    formatData: function () {

        var chartResponseObj = {
            categories: [],
            data: []
        };

        var globalMap = new Map(), dataMap = new Map(), dataArr = [], categoryArr = [];

        if (this.responseData != null) {
            this.responseData.forEach(function (val, key) {

                if (dataMap.get(val[Object.keys(val)[1]]) != null) {
                    globalMap.set(val[Object.keys(val)[0]],
                        val[Object.keys(val)[2]]);
                    dataMap.set(val[Object.keys(val)[1]],
                        globalMap);
                } else {
                    globalMap = new Map();
                    dataArr = [];
                    dataArr.push(val[Object.keys(val)[2]]);
                    globalMap.set(val[Object.keys(val)[0]],
                        val[Object.keys(val)[2]]);
                    dataMap.set(val[Object.keys(val)[1]],
                        globalMap);
                }

                categoryArr.push(val[Object.keys(val)[0]]);
            });
        } else {
            console.log("DataFormatter :: No data returned from server");
        }

        categoryArr = categoryArr.filter(this.getUniqueValues);

        dataMap.forEach(function (value, key) {
            var dataObj = {
                name: "",
                data: []
            };

            dataObj.name = key;

            for (category in categoryArr) {
                if (value.get(categoryArr[category]))
                    dataObj.data.push((typeof value.get(categoryArr[category]) == "string") ? parseInt(value.get(categoryArr[category])) : value.get(categoryArr[category]));
                else
                    dataObj.data.push(null);
            }

            chartResponseObj.data.push(dataObj);
        });

        chartResponseObj.categories.push(categoryArr);

        return chartResponseObj;
    }
};

// Data Formatter for scatterplot charts
// Expected source data format
/*  var sourceData = {
        "data": [
            {
                "name": "Female",
                "height": 10,
                "weight": 100
            },
            {
                "name": "Female",
                "height": 10,
                "weight": 200
            },
            {
                "name": "Male",
                "height": 8,
                "weight": 150
            },
            {
                "name": "Male",
                "height": 10,
                "weight": 190
            }

        ]
    };
*/
var ScatterPlotDataFormatter;

ScatterPlotDataFormatter = {
    responseData: "",
    getUniqueValues: function (value, index, self) {
        return self.indexOf(value) === index;
    },
    formatData: function () {

        var chartResponseObj, chartResponseArr = [] , categoryMap = new Map(), dataArr = [];

        this.responseData.forEach(function (value, key) {

            var dataElementArr = [];

            if(categoryMap.get(value[Object.keys(value)[0]]) != null){

                dataElementArr.push(value[Object.keys(value)[1]]);
                dataElementArr.push(value[Object.keys(value)[2]]);

                categoryMap.get(value[Object.keys(value)[0]]).push(dataElementArr);
            }else{
                dataArr = [];

                dataElementArr.push(value[Object.keys(value)[1]]);
                dataElementArr.push(value[Object.keys(value)[2]]);

                dataArr.push(dataElementArr);

                categoryMap.set(value[Object.keys(value)[0]],dataArr);
            }
        });

        categoryMap.forEach(function(value,key){

            chartResponseObj = {
                name: "",
                data: []
            };

            chartResponseObj.name = key;
            chartResponseObj.data = value;

            chartResponseArr.push(chartResponseObj);
        });

        return chartResponseArr;
    }
};


// DataFomatting method which converts raw data to
// chart data with the help of formatters
var formatData;
formatData = function (objectToConvert, dataFormatter) {

    var responseObject;
    responseObject = Object.create(dataFormatter);
    responseObject.responseData = objectToConvert;

    return responseObject.formatData();
};